# Get started

## Requirements

> MyStation is designed to work on a [Raspberry Pi](https://raspberrypi.org/) controller.
> It requires some components and functionalities like `GPIO` connectors.

You can build your MyStation server yourself. To get more informations about it, go to [Do It Yourself](/v1.0/doc/diy)!

## Installation

This step is almost the same that the default Raspberry Pi installation procedure.

All you need is an SD card and a software like *Raspberry Pi Imager* to write the image on. (Download RPi Imager at [https://www.raspberrypi.org/downloads/](https://www.raspberrypi.org/downloads/))

> We recommend strongly to choose an SD card having more than **16GB** of capacity.

Ok, [download the image](http://mystation.fr/download) and write it on the SD card!

When everything is fine, insert the SD card into the Raspberry and boot :)

## First login

At your first login on MyStation, you will must reset the `admin` password.

Default credentials:
- username: `admin`
- password: `admin`

::: warning
MyStation has a default `admin` user that has all privileges. Be careful when you logged as and choose a **strong** password!
:::
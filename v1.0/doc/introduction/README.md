# Introduction

This documentation is destined to MyStation users.

> You are an *advanced* user? Or a *developer*?
> 
> So, please read the MyStation guide for developers:
> 
> [http://developers.mystation.fr/](http://developers.mystation.fr/)

Everything is fine? [Let's go](/v1.0/doc/get-started)!
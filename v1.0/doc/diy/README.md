# Do It Yourself

::: warning
You must have some knowledges and tools to build your own MyStation server. But it should not be impossible ;)
Read the guide and 
:::

> Don't forget that you can contact and chat with us on the official Discord server: [here](https://discord.com/channels/729028505123291228)

## Sango server

The `Sango` server is the main MyStation 'case'. This is the 'heart' of MyStation.

The case is 3D-printable. The STL files can be found at [http://mystation.fr/#/resources/](http://mystation.fr/#/resources/)

### Motherboard

Raspberry Pi model 3B+ (no previous model tested).

### Components

Coming soon...

## Modules

Not yet available... :(
# Introduction

Cette documentation est destinée aux utilisateurs de MyStation.

> Vous êtes un utilisateur *avancé*? Ou *développeur*?
> 
> Consultez donc la documentation de MyStation pour développeurs:
> 
> [http://developers.mystation.fr/](http://developers.mystation.fr/)


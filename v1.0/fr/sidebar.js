module.exports = {
  '/fr/doc/': [
    {
      title: 'Introduction',
      path: '/fr/doc/introduction/'
    },
    {
      title: 'Démarrer',
      path: '/fr/doc/get-started/'
    },
    {
      title: 'DIY',
      path: '/fr/doc/diy/'
    },
    {
      title: 'Avancé',
      path: '/fr/doc/advanced'
    }
  ]
}
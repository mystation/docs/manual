---
home: true
heroImage: http://mystation.fr/static/img/logo.png
heroText: MyStation
tagline: Manuel utilisateur
actionText: Démarrer →
actionLink: /doc/introduction/
footer: GPL v3 Licensed | Copyright © 2019-present Hervé Perchec
---
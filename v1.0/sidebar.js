module.exports = {
  '/doc/': [
    {
      title: 'Introduction',
      path: '/doc/introduction/'
    },
    {
      title: 'Get started',
      path: '/doc/get-started/'
    },
    {
      title: 'DIY',
      path: '/doc/diy/'
    },
    {
      title: 'Advanced',
      path: '/doc/advanced'
    }
  ]
}